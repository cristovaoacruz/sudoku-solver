#define QUICKNESS 1 /*100000 /* us */

#define _BSD_SOURCE /* snprintf(), usleep() */

#include <stdbool.h>         /* bool */
#include <stddef.h>          /* NULL */
#include <stdio.h>           /* *printf(), stderr */
#include <stdlib.h>          /* EXIT_SUCCESS */
#include <string.h>          /* memset() */
#include <unistd.h>          /* usleep() */
#include <X11/Xlib.h>

struct crd {
	int br; /* big row */
	int bc; /* big column */
	int sr; /* small row */
	int sc; /* small column */
};

#define BIG_GRD_N 3 /* side of the big grid, in squares */
#define SML_GRD_N 3 /* side of the small grid, in squares */

const int initial_height = 480;
const int initial_width = 480;

#define SML_GRD_N_HOR_SEG ((SML_GRD_N - 1) * (BIG_GRD_N))
#define SML_GRD_N_VER_SEG (SML_GRD_N_HOR_SEG)
#define SML_GRD_N_SEG (SML_GRD_N_HOR_SEG + SML_GRD_N_VER_SEG)

XSegment sml_grd[SML_GRD_N_SEG];
XSegment *sml_grd_ver = sml_grd;
XSegment *sml_grd_hor = sml_grd + SML_GRD_N_VER_SEG;

#define BIG_GRD_N_HOR_SEG (BIG_GRD_N - 1)
#define BIG_GRD_N_VER_SEG (BIG_GRD_N_HOR_SEG)
#define BIG_GRD_N_SEG (BIG_GRD_N_HOR_SEG + BIG_GRD_N_VER_SEG)

XSegment big_grd[BIG_GRD_N_SEG];
XSegment *big_grd_ver = big_grd;
XSegment *big_grd_hor = big_grd + BIG_GRD_N_VER_SEG;

char numbers[BIG_GRD_N*BIG_GRD_N*SML_GRD_N*SML_GRD_N];
const int numbers_count = sizeof(numbers) / sizeof(numbers[0]);

const char *font_name = "-misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-1";

Display *display;
Window window;
GC gc;
Font f;
int height, width, scale;

struct crd search_pattern[BIG_GRD_N*BIG_GRD_N*SML_GRD_N*SML_GRD_N];

void draw_number(char number, struct crd x);
void set_number(struct crd x, char number);
char get_number(struct crd x);
int get_number_idx(struct crd x);
struct crd get_number_crd(int idx);
bool check_number(struct crd x);
void fill_search_pattern(void);
bool process_events(void);

void make_window(void)
{
	XGCValues values;

	display = XOpenDisplay(NULL);
	window = XCreateSimpleWindow(
				display,
				DefaultRootWindow(display),
				0, 0, height, width,
				0, 0,
				BlackPixel(display, DefaultScreen(display))
				);
	gc = XCreateGC(display, window, 0, &values);

	XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
	XSelectInput(display, window, StructureNotifyMask | ButtonPress | KeyPress);

	f = XLoadFont(display, font_name);
	XSetFont(display, gc, f);

	XClearWindow(display, window);
	XMapRaised(display, window);
}

void wait_window(void)
{
  XEvent e;

  do {
      XNextEvent(display, &e);
  } while(e.type != MapNotify);
}


void free_window(void)
{
	XFreeGC(display, gc);
	XDestroyWindow(display, window);
	XUnloadFont(display, f);
	XCloseDisplay(display);
}


/*
void make_pallete(void)
{
	XAllocNamedColor(
		display,
		DefaultColormap(display, DefaultScreen(display)),
		"blue",
		&blue,
		&blue
	);
}
*/


void draw_board(void)
{
	int r;
	int col;

	/* Compute the inner vertical segments of the small grid. */
	for (r = 0; r < SML_GRD_N_VER_SEG; ++r) {
		col = (r / (SML_GRD_N - 1)) * SML_GRD_N + (r % (SML_GRD_N - 1)) + 1;
		sml_grd_ver[r].x1 = col * scale;
		sml_grd_ver[r].y1 = 0;
		sml_grd_ver[r].x2 = col * scale;
		sml_grd_ver[r].y2 = height;
	}
	/*
	* Compute the inner horizontal segments of the small grid, by
	* transposing the vertical ones.
	*/
	for (r = 0; r < SML_GRD_N_HOR_SEG; ++r) {
		sml_grd_hor[r].x1 = sml_grd_ver[r].y1;
		sml_grd_hor[r].y1 = sml_grd_ver[r].x1;
		sml_grd_hor[r].x2 = sml_grd_ver[r].y2;
		sml_grd_hor[r].y2 = sml_grd_ver[r].x2;
	}

	/* Compute the inner vertical segments of the big grid. */
	for (r = 0; r < BIG_GRD_N_HOR_SEG; ++r) {
		col = (r + 1) * SML_GRD_N;
		big_grd_ver[r].x1 = col * scale;
		big_grd_ver[r].y1 = 0;
		big_grd_ver[r].x2 = col * scale;
		big_grd_ver[r].y2 = height;
	}
	/*
	* Compute the inner horizontal segments of the big grid, by
	* transposing the vertical ones.
	*/
	for (r = 0; r < BIG_GRD_N_VER_SEG; ++r) {
		big_grd_hor[r].x1 = big_grd_ver[r].y1;
		big_grd_hor[r].y1 = big_grd_ver[r].x1;
		big_grd_hor[r].x2 = big_grd_ver[r].y2;
		big_grd_hor[r].y2 = big_grd_ver[r].x2;
	}

	/* Draw the thin lines. */
	XSetLineAttributes(display, gc, 1, LineSolid, CapButt, JoinMiter);
	XDrawSegments(display, window, gc, sml_grd, SML_GRD_N_SEG);

	/* Draw the thick inner lines. */
	XSetLineAttributes(display, gc, 2, LineSolid, CapButt, JoinMiter);
	XDrawSegments(display, window, gc, big_grd, BIG_GRD_N_SEG);

	/* Draw the thick outer lines, the border. */
	XDrawRectangle(display, window, gc, 1, 1, width-2, height-2);
}

void draw_numbers(void)
{
	int r;
	struct crd x;

	for (r = 0; r < numbers_count; ++r) {
		x = get_number_crd(r);
		draw_number(numbers[r], x);
	}
}

void draw_number(char number, struct crd x)
{
	char str[2];
	int row, col;
	int tx, ty;
	int rx, ry, rw, rh;
	int clear_margin = 6;

	if (number > 0) {
		(void)snprintf(str, 2, "%d", number);
	} else {
		(void)snprintf(str, 2, " ");
	}

	/* Compute coordinates for the center of the destination cell. */
	row = x.br * SML_GRD_N + x.sr;
	col = x.bc * SML_GRD_N + x.sc;
	tx = col * scale + scale / 2;
	ty = height - row * scale - scale / 2;

	/* Clear cell first. */
	rx = col * scale + clear_margin;
	ry = height - (row + 1) * scale + clear_margin;
	rw = scale - 2 * clear_margin;
	rh = scale - 2 * clear_margin;
	XClearArea(display, window, rx, ry, rw, rh, false);

	XDrawString(display, window, gc, tx, ty, str, 1);
}

void set_number(struct crd x, char number)
{
	int idx;

	idx = get_number_idx(x);
	numbers[idx] = number;
}

char get_number(struct crd x)
{
	int idx;

	idx = get_number_idx(x);
	return numbers[idx];
}

int get_number_idx(struct crd x)
{
	int row, col, idx;

	row = x.br * SML_GRD_N + x.sr;
	col = x.bc * SML_GRD_N + x.sc;
	idx = row * BIG_GRD_N * SML_GRD_N + col;

	return idx;
}

struct crd get_number_crd(int idx)
{
	int row, col;
	int ncols;
  struct crd x;

	ncols = (BIG_GRD_N * SML_GRD_N);
	row = idx / ncols;
	col = idx % ncols;

	x.br = row / SML_GRD_N;
	x.bc = col / SML_GRD_N;
	x.sr = row % SML_GRD_N;
	x.sc = col % SML_GRD_N;

  return x;
}

bool check_number(struct crd x)
{
	struct crd xx;
	char me, you;

	me = get_number(x);

	/* Check row. */
  xx = x;
	for (xx.bc = 0; xx.bc < BIG_GRD_N; ++xx.bc) {
		/* Do not check this block. */
		if (xx.bc == x.bc) {
			continue;
		}

		for (xx.sc = 0; xx.sc < SML_GRD_N; ++xx.sc) {
			you = get_number(xx);
			if (me == you) {
				return false;
			}
		}
	}

	/* Check column. */
  xx = x;
	for (xx.br = 0; xx.br < BIG_GRD_N; ++xx.br) {
		/* Do not check this block. */
		if (xx.br == x.br) {
			continue;
		}

		for (xx.sr = 0; xx.sr < SML_GRD_N; ++xx.sr) {
			you = get_number(xx);
			if (me == you) {
				return false;
			}
		}
	}

	/* Check block. */
  xx = x;
	for (xx.sr = 0; xx.sr < SML_GRD_N; ++xx.sr) {
		for (xx.sc = 0; xx.sc < SML_GRD_N; ++xx.sc) {
      /* Do not check this cell. */
			if ((xx.sr == x.sr) && (xx.sc == x.sc)) {
				continue;
			}

			you = get_number(xx);
			if (me == you) {
				return false;
			}
		}
	}

	return true;
}

void fill_search_pattern(void)
{
	int r;
	int n = sizeof(search_pattern) / sizeof(search_pattern[0]);

	int sn = SML_GRD_N * SML_GRD_N;

	for (r = 0; r < n; ++r) {
		search_pattern[r].br = (r / sn) / BIG_GRD_N;
		search_pattern[r].bc = (r / sn) % BIG_GRD_N;
		search_pattern[r].sr = (r % sn) / SML_GRD_N;
		search_pattern[r].sc = (r % sn) % SML_GRD_N;
	}
}

/*
void setup_board(void)
{
	const int n = sizeof(board) / sizeof(board[0]);
	struct crd x;
	int r;

	for (r = 0; r < n; ++r) {
		x = board_crd[r];
		set_number(x, board[r]);
	}
}
*/

bool process_events(void)
{
	static int counter = 0;
	XEvent e;
	XWindowAttributes a;

	XGetWindowAttributes(display, window, &a);
	/*
	XNextEvent(display, &e);
	*/
	while (XCheckWindowEvent(display, window, a.your_event_mask, &e)) {
		switch (e.type) {
			case KeyPress:
				counter++;
				break;
			case ButtonPress:
				counter++;
				break;
			case MapNotify:
				printf("I'm on the map!\n");
				break;
			default:
				fprintf(stderr, "Unrecognized event %d.\n", e.type);
				break;
		}
	}

	if (counter > 1) {
		return true;
	}

	return false;
}

bool solve(int r)
{
	const int n = sizeof(search_pattern) / sizeof(search_pattern[0]);

	char number;
	bool solved, possible;
	struct crd x;

	solved = false;

	if (r >= n) {
		/* Solved. */
		return true;
	}

	if (process_events()) {
		/* Not solved, but user requested exit. */
		return true;
	}

	x = search_pattern[r];

	for (number = 1; (number <= 9) && (!solved); ++number) {
		set_number(x, number);

		possible = check_number(x);
		if (possible) {
			/* Here we have a valid board, draw it. */
			draw_numbers();
			usleep(QUICKNESS);

      /* Down the rabit hole. */
			solved = solve(r + 1);
		}
	}

	/* Clean our guess if no valid number was found. */
	if (!solved) {
		set_number(x, 0);
	}

	return solved;
}

int main(void)
{
	height = initial_height;
	width = initial_width;
	scale = width / (SML_GRD_N * BIG_GRD_N);

	(void)memset(numbers, 0, sizeof(numbers));

	make_window();
  wait_window();

	draw_board();
	fill_search_pattern();

  XSync(display, False);

  /*
	setup_board();
	*/

	solve(0);

	/*
	XSync(display, False);
	*/

	/* Wait until user exits. */
	while (!process_events()) {
		usleep(500000);
	}

	free_window();
	
	return EXIT_SUCCESS;
}
