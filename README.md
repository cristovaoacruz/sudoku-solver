= Sudodu solver

A sudoku solver that uses a backtracking approach and provides a visualization
for the whole process.
